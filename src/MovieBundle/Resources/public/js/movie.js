(function(yourcode) {
    yourcode(window.jQuery, window, document);
} (function($, window, document) {
    $(function() {

        var movie = (function() {
            initAjaxPaginate = function() {
                $('body').on('click', '.ajax-paginate', function(e){
                    var url = $(this).attr('href');

                    initAjaxRefresh(url);

                    e.preventDefault();
                });
            },

            initAjaxDelete = function() {
                $('body').on('click', '.ajax-delete', function(e){
                    var url = $(this).attr('href');

                    initAjaxRefresh(url);

                    e.preventDefault();
                });
            },

            initAjaxRefresh = function(url) {
                $.get(url, function(response){
                    $('section.list').remove();
                    $('section.parent').after(response.template);
                }, 'json');
            },

            initSubmit = function() {
                $('input,select,textarea').jqBootstrapValidation({
                    submitSuccess: function ($form, event) {
                        var url = $form.attr('action');
                        var data = $form.serialize();

                        $form.find('button').attr('disabled', 'disabled');

                        $.post(url, data, function(response){
                            initAjaxRefresh($form.data('refresh-url'));
                        }, 'json').done(function(){
                            $form.find('button').removeAttr('disabled');
                            $form[0].reset();
                        });
                        event.preventDefault();
                    }
                });
            },

            init = function() {
                initSubmit();
                initAjaxPaginate();
                initAjaxDelete();
            };

            return {
                init: init
            }
        })();
        movie.init();
    });
}));