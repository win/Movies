<?php
namespace MovieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MovieBundle\Repository\MovieRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="movie")
 */
class Movie
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="integer")
     */
    protected $year;

    /**
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @ORM\Column(type="string")
     */
    protected $ipPublisher;

    /**
     * @ORM\Column(type="float")
     */
    private $rating;

    /**
     * @ORM\PrePersist
     */
    public function setIpPublisherValue()
    {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
        $this->ipPublisher = $ipaddress;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Movie
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return Movie
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Movie
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set ipPublisher
     *
     * @param string $ipPublisher
     * @return Movie
     */
    public function setIpPublisher($ipPublisher)
    {
        $this->ipPublisher = $ipPublisher;

        return $this;
    }

    /**
     * Get ipPublisher
     *
     * @return string 
     */
    public function getIpPublisher()
    {
        return $this->ipPublisher;
    }


    /**
     * Set rating
     *
     * @param float $rating
     * @return Movie
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return float 
     */
    public function getRating()
    {
        return $this->rating;
    }
}
