<?php
namespace MovieBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MovieFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array(
                'label' => false,
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Title',
                    'data-validation-required-message' => 'You must enter title',
                    'class' => 'form-control'
                )
            ))
            ->add('year', null, array(
                'label' => false,
                'required' => false,
                'attr' => array(
                    'placeholder' => 'Year',
                    'class' => 'form-control',
                    'data-validation-required-message' => 'You must enter year',
                )
            ))
            ->add('description', null, array(
                'label' => false,
                'required' => false,
                'attr' => array(
                    'placeholder' => 'Description',
                    'rows' => 5,
                    'cols' => 83,
                    'class' => 'form-control',
                    'data-validation-required-message' => 'You must enter description',
                )
            ))
            ->add('rating', null, array(
                'label' => false,
                'required' => false,
                'attr' => array(
                    'placeholder' => 'Rating',
                    'class' => 'form-control',
                    'data-validation-required-message' => 'You must enter rating',
                )
            ))
            ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MovieBundle\Entity\Movie',
            'cascade_validation' => false,
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return 'sendingFeedback';
    }
}