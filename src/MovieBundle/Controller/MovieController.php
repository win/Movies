<?php

namespace MovieBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MovieBundle\Form\Type\MovieFormType;
use MovieBundle\Entity\Movie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MovieController extends Controller
{
    /**
     * @Route("/movies/{page}", name="movies", defaults={"page" = 1})
     */
    public function indexAction(Request $request, $page)
    {
        $movie = new Movie();
        $form = $this->createForm(new MovieFormType(), $movie);

        $moviesQuery = $this->getDoctrine()->getManager()->getRepository('MovieBundle:Movie')->findAllOrderedById();

        $paginatorMovies = $this->get('knp_paginator');
        $paginationMovies = $paginatorMovies->paginate(
            $moviesQuery,
            $request->query->get('page', $page),
            $this->container->getParameter('movies_per_page')
        );

        $paginationMovies->setTemplate('MovieBundle:Movie:pagination.html.twig');
        $data = array(
            'form' => $form->createView(),
            'movies' => $paginationMovies
        );
        return $this->render('MovieBundle:Movie:index.html.twig', array('data' => $data));
    }

    /**
     * @Route("/save-movie", name="save_movie")
     */
    public function saveMovieAction(Request $request)
    {
        $movie = new Movie();
        $form = $this->createForm(new MovieFormType(), $movie);
        $status = false;

        $form->submit($request->request->get($form->getName()));
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($movie);
            $em->flush();
            $status = true;
        }

        $data = array(
            'status' => $status
        );

        return new Response(json_encode($data));
    }

    /**
     * @Route("/get-movies/{page}", name="get_movies", defaults={"page" = 1})
     */
    public function getListAction(Request $request, $page)
    {
        $moviesQuery = $this->getDoctrine()->getManager()->getRepository('MovieBundle:Movie')->findAllOrderedById();

        $paginatorMovies = $this->get('knp_paginator');
        $paginationMovies = $paginatorMovies->paginate(
            $moviesQuery,
            $request->query->get('page', $page),
            $this->container->getParameter('movies_per_page')
        );
        $paginationMovies->setTemplate('MovieBundle:Movie:pagination.html.twig');

        $data = array(
            'movies' => $paginationMovies
        );

        $template = $this->renderView('MovieBundle:Movie:list.html.twig', array(
            'data' => $data
        ));

        return new JsonResponse(array(
            'template' => $template
        ));
    }

    /**
     * @Route("/delete-movie/{id}", name="delete_movie")
     */
    public function deleteMovieAction(Request $request, $id)
    {
        $movie = $this->getDoctrine()->getManager()->getRepository('MovieBundle:Movie')->find($id);

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($movie);
        $em->flush();

        $moviesQuery = $this->getDoctrine()->getManager()->getRepository('MovieBundle:Movie')->findAllOrderedById();

        $paginatorMovies = $this->get('knp_paginator');
        $paginationMovies = $paginatorMovies->paginate(
            $moviesQuery,
            $request->query->get('page', 1),
            $this->container->getParameter('movies_per_page')
        );
        $paginationMovies->setTemplate('MovieBundle:Movie:pagination.html.twig');

        $data = array(
            'movies' => $paginationMovies
        );

        $template = $this->renderView('MovieBundle:Movie:list.html.twig', array(
            'data' => $data
        ));

        return new JsonResponse(array(
            'template' => $template
        ));
    }
}
