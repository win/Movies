CREATE DATABASE movie;

CREATE TABLE IF NOT EXISTS `movie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ipPublisher` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rating` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `movie`
--

INSERT INTO `movie` (`id`, `title`, `year`, `description`, `ipPublisher`, `rating`) VALUES
(1, 'test1', 24233, 'fdsfdsf1', '127.0.0.1', 444),
(3, 'test2', 24231, 'fdsfdsf2', '127.0.0.1', 444),
(4, 'test3', 24214, 'fdsfdsf2', '127.0.0.1', 444),
(5, 'test4', 24214, 'fdsfdsf3', '127.0.0.1', 444),
(6, 'test5', 24214, 'fdsfdsf4', '127.0.0.1', 444),
(7, 'test6', 24214, 'fdsfdsf5', '127.0.0.1', 444),
(11, 'test7', 24214, 'fdsfdsf6', '127.0.0.1', 444),
(17, 'sdfsd', 1234234, 'sfsdfsdf8', '127.0.0.1', 4234),
(20, 'wrewe7r', 4234, 'fdsfds7f', '127.0.0.1', 424);